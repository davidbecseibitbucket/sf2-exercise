Javaslatok: 
===========

   - Az üres return utasítások helyett exceptionokat kell dobni.

   - getClientId: Az ifet kellene átszervezni. Legalább a feltételt. De az egymásba ágyazás sem         feltételenül a legjobb.     

A retun null felesleges.



Symfony tesztfeladat
====================

A repository tulajdonképpen egy sf3 standard edition-t tartalmaz, 
amihez hozzá lett adva egy funkció: az url-ek megtekintésének számolása, 
és azok adatbázisban történő letárolása.  

A feladat
---------

A végrehajtandó feladat a következő

  * értelmezd a ViewCount tábla adatszerkezetét, a letárolt adatokat
  * ajánlj módosítási javaslatokat a kóddal kapcsolatosan (mit csinálnál másképp)
  * készíts megoldást arra, hogy a táblában lévő adatokat napokra aggregált formára hozzuk: 
    * ne legyen minden session-höz és IP címhez külön-külön rekord a táblában, hanem minden nap, 
  minden url-hez csak napi (timespan mező) összeg.
    * az oldalon megjelenített adatnak nyilván nem szabad változni az aggregálás futtatását követően