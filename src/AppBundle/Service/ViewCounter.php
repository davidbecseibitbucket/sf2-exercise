<?php

namespace AppBundle\Service;


use AppBundle\Entity\ViewCount;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ViewCounter
{
    use ContainerAwareTrait;

    private $_em;
    private $_request;

    public function __construct(EntityManager $entityManager, RequestStack $requestStack)
    {
        $this->_em = $entityManager;
        $this->_request = $requestStack->getCurrentRequest();
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $slug = $event->getRequest()->attributes->get('slug', false);

        if (!$slug) {
            return;
        }

        $this->storeView($slug);
    }

    private function storeView($url)
    {
        $clientId = $this->getClientId();

        if (!$clientId) {
            return;
        }

        $start = new \DateTime(date('Y-m-d'));

        $viewCount = $this->_em->getRepository('AppBundle:ViewCount')->findOneBy(
             ['start' => $start, 'url' => $url, 'client' => $clientId]
        );

        if (!$viewCount) {
            $viewCount = new ViewCount();
            $viewCount->setUrl($url);
            $viewCount->setStart($start);
            $viewCount->setTimespan(3600);
            $viewCount->setClient($clientId);
            $viewCount->setCount(1);
        } else {
            $viewCount->setCount($viewCount->getCount() + 1);
            $viewCount->setTimeSpan($viewCount->getTimeSpan() + 3600);
        }

        $this->_em->persist($viewCount);
        $this->_em->flush();
    }

    public function fetchViewCountForUrl($url)
    {
        $slug = ltrim($url, '/');

        $sumOfViews = $this->_em->getRepository('AppBundle:ViewCount')->createQueryBuilder('q')
            ->select('sum(q.count)')
            ->where('q.url = :url')
            ->setParameter('url', $slug)
            ->getQuery()
            ->getSingleResult();

        return $sumOfViews[1];
    }

    private function getClientId()
    {
        if ($this->_request->getSession() && $this->_request->getSession()->getId()) {
            return $this->_request->getSession()->getId();
        } else {
            $ip = explode('.', $this->_request->getClientIp());
            if (4 === count($ip)) { // only if ipv4
                $ip[1] = 'xxx'; //avoid storing complete IP addresses in database
            }
            return implode('.', $ip);
        }

        return null;
    }


}