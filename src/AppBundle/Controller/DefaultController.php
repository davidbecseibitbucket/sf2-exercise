<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/{slug}", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $viewCounter = $this->get('app.view_counter');
        $viewCount = $viewCounter->fetchViewCountForUrl($request->get('slug'));

        return $this->render('default/index.html.twig', [
            'base_dir'      => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'view_count'    => $viewCount,
        ]);
    }
}
